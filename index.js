const TelegramBot = require('node-telegram-bot-api');

const token = 'TOKEN';
const bot = new TelegramBot(token, {polling:true});
const cards = require('./cards.json');
const questions = getOptions('Q');
const answers = getOptions('A');

function getOptions(option){
    var quest = [];
    cards.forEach(element => {
        if (element.cardType == option){
            quest.push(element)
        }
    });
    return quest;
}

bot.onText(/\/start/, (msg) => {
    bot.sendMessage(
        msg.chat.id, 'commands:\n'+
        '/question\n'+
        '/amount');
})

bot.onText(/\/question/, (msg) => {
    var id = Math.floor(Math.random() * (Object.keys(questions).length - 0));
    bot.sendMessage(
        msg.chat.id, questions[id].text);

    for (let index = 0; index < questions[id].numAnswers; index++) {
        var answ = Math.floor(Math.random() * (Object.keys(answers).length - 0));
        bot.sendMessage(
            msg.chat.id, answers[answ].text
        );
        
    }
//    bot.sendMessage(
//        msg.chat.id, 'Tengo '+Object.keys(questions).length+' Cartas');
})

bot.onText(/\/amount/, (msg) => {
    bot.sendMessage(
        msg.chat.id, 'Tengo '+Object.keys(cards).length+' Cartas');
})